/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include "pagesessions.h"
#include "ui_pagesessions.h"

#include <QFuture>
#include <QToolButton>
#include <QtConcurrent/QtConcurrent>
#include <QMessageBox>
#include <QScroller>
#include <QScreen>

#include "sessionsavedialog.h"

#include <cprime/cprime.h>
#include <cprime/appopenfunc.h>
#include <cprime/messageengine.h>


pagesessions::pagesessions(QWidget *parent) :QWidget(parent)
  , ui(new Ui::pagesessions)
  , smi(new settings)
{
    ui->setupUi(this);

    loadSettings();
    startSetup();
    setupSessionsView();
}

pagesessions::~pagesessions()
{
	delete smi;
    delete ui;
}

void pagesessions::reload()
{
    setupSessionsView();
}

void pagesessions::addControls(QList<QToolButton *> controls)
{
    menu = controls[0];
    m_addSession = controls[1];
    m_editSession = controls[2];
    m_deleteSession = controls[3];

    connect(menu, &QToolButton::toggled, this, &pagesessions::menuClicked);
}

/**
 * @brief Loads application settings
 */
void pagesessions::loadSettings()
{
    listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");
    uiMode = smi->getValue("CoreApps", "UIMode");
    useSystemNotification = smi->getValue("CoreApps", "UseSystemNotification");
}

void pagesessions::startSetup()
{
    ui->sessionsView->setIconSize( listViewIconSize );

    if ( uiMode != 0 ) {
        QScroller::grabGesture(ui->sessionsView, QScroller::LeftMouseButtonGesture);
        connect( ui->sessionsView,SIGNAL( itemClicked( QTreeWidgetItem*,int ) ),this,SLOT( openSelectedSession( QTreeWidgetItem*,int ) ) );
    } else {
        connect( ui->sessionsView,SIGNAL( itemDoubleClicked( QTreeWidgetItem*,int ) ),this,SLOT( openSelectedSession( QTreeWidgetItem*,int ) ) );
    }
}

void pagesessions::setupSessionsView()
{
    ui->sessionsView->clear();

    QSettings *session = new QSettings( CPrime::Variables::CC_System_ConfigDir() + "coreapps/session", QSettings::IniFormat );

    QStringList nameList = session->childGroups();
    nameList = CPrime::SortFunc::sortList( nameList );

//    QFuture<void> f = QtConcurrent::run( [this, session, &nameList]() {

        foreach ( QString name, nameList ) {
            // Begin Session name group
            session->beginGroup( name );

            QTreeWidgetItem *nameTree = new QTreeWidgetItem;
            nameTree->setText( 0, name );
            nameTree->setIcon(0,CPrime::ThemeFunc::themeIcon( "object-select-symbolic", "object-group", "object-select" ));
            nameTree->setSizeHint( 0, listViewIconSize );

            QStringList appNames = session->childGroups();

            foreach ( QString appName, appNames ) {
                // Begin date time group
                session->beginGroup( appName );

                if ( session->childKeys().count() >= 1 ) {
                    QTreeWidgetItem *appNameT = new QTreeWidgetItem;
                    appNameT->setText( 0, appName );
                    appNameT->setIcon( 0, CPrime::ThemeFunc::getAppIcon( appName.toLower() ) );

                    QStringList keys = session->childKeys();

                    foreach ( QString key, keys ) {
                        QString value = session->value( key ).toString();

                        if ( value.length() ) {
                            QTreeWidgetItem *child = new QTreeWidgetItem;
                            child->setText( 0, value );
                            child->setIcon( 0, value.length() ? CPrime::ThemeFunc::getAppIcon( value.toLower() ) :
                                          CPrime::ThemeFunc::getAppIcon( appName.toLower() ) );
                            appNameT->addChild( child );
                        }
                    }

                    nameTree->addChild( appNameT );
                }

                session->endGroup();

            }

            session->endGroup();
            int index = ui->sessionsView->topLevelItemCount();
            ui->sessionsView->insertTopLevelItem( index, nameTree );
        }
//    } );

//    qRegisterMetaType<QVector<int>>( "QVector<int>" );

//  QFutureWatcher<void> *r = new QFutureWatcher<void>();
//  r->setFuture( f );
//  connect( r, &QFutureWatcher<void>::finished, [this]() {
    if ( ui->sessionsView->model()->hasIndex( 0, 0 ) ) {
      ui->sessionsView->setExpanded( ui->sessionsView->model()->index( 0, 0 ), true );
    }
//      } );

    if(ui->sessionsView->topLevelItemCount()){
        ui->sessionsView->setVisible(1);
        ui->welcome->setVisible(0);
    } else{
        ui->sessionsView->setVisible(0);
        ui->welcome->setVisible(1);
    }
}

void pagesessions::addSession()
{
    sessionSaveDialog *ssd = new sessionSaveDialog( 0, "" );

    if ( ssd->exec() ) {
        setupSessionsView();
    }
}

void pagesessions::deleteSession()
{
	if (!ui->sessionsView->currentItem()) {
		return;
	}

    QString msg = QString( "Do you want to delete the \"%1\" Session?" ).arg( ui->sessionsView->currentItem()->text( 0 ) );
    QMessageBox message( QMessageBox::Question, tr( "Confirmation" ), msg, QMessageBox::Yes | QMessageBox::No );
    message.setWindowIcon( QIcon::fromTheme( "cc.cubocore.CoreStuff" ) );
    int merge = message.exec();

    if ( merge == QMessageBox::Cancel ) {
        return;
    }

    if ( merge == QMessageBox::Yes ) {
        if ( ui->sessionsView->currentItem() ) {
            // FIX ME : Add session file to variable.
            QSettings session( CPrime::Variables::CC_System_ConfigDir() + "coreapps/session", QSettings::IniFormat );
            QStringList sessionList = session.childGroups();
            QString selected = ui->sessionsView->currentItem()->text( 0 );

            if ( sessionList.contains( selected ) ) {
                session.remove( selected );
                ui->sessionsView->takeTopLevelItem( ui->sessionsView->currentIndex().row() );
            }
        }

		CPrime::MessageEngine::showMessage("cc.cubocore.CoreStuff", "CoreStuff", "Session Removed", "Selected session is removed.");
    }
}

void pagesessions::editSessoion()
{
	if (!ui->sessionsView->currentItem()) {
		return;
	}

    QString sessionName = ui->sessionsView->currentItem()->text( 0 );
    sessionSaveDialog *ssd = new sessionSaveDialog( 1, sessionName );

    if ( ssd->exec() ) {
        setupSessionsView();
    }
}

void pagesessions::openSelectedSession( QTreeWidgetItem *item, int column )
{
    QStringList nameList;
    QSettings session( CPrime::Variables::CC_System_ConfigDir() + "coreapps/session", QSettings::IniFormat );
    QStringList group = session.childGroups();

    foreach ( QString s, group ) {
        session.beginGroup( s );
        nameList.append( s );
        session.endGroup();
    }

    QString selected = item->text( column );

    if ( nameList.contains( selected ) ) {
        QFuture<void> future = QtConcurrent::run( [&, item]() {
            for ( int i = 0; i < item->childCount(); i++ ) {

                QTreeWidgetItem *midChildT = item->child( i );

                if ( midChildT->childCount() ) {
                    for ( int j = 0; j < midChildT->childCount(); j++ ) {
                        QString command = midChildT->child( j )->text( 0 );
                        QString arg = "";
                        if (command.contains("-e ")) {
                            command = command.replace("-e ", "");
                            arg = "-e";
                        }

                        QStringList args;
                        if (arg.length()) {
                            args << arg;
                        }
                        args << command;
                        CPrime::AppOpenFunc::systemAppOpener( midChildT->text( 0 ), args);
                        QThread::currentThread()->msleep( 1000 );
                    }
                } else {
                    // Need to fix session
                    CPrime::AppOpenFunc::systemAppOpener( midChildT->text( 0 ) );
                    QThread::currentThread()->msleep( 1000 );
                }
            }
        } );

        QFutureWatcher<void> *f = new QFutureWatcher<void>();
        f->setFuture( future );
        connect( f, &QFutureWatcher<void>::finished, this, &pagesessions::showSessionMessage );
    }
}

void pagesessions::showSessionMessage()
{
	CPrime::MessageEngine::showMessage("cc.cubocore.CoreStuff", "CoreStuff", "Session restored successfully", "Selected Session is restored successfully" );
}

void pagesessions::menuClicked()
{
    on_sessionsView_itemSelectionChanged();
}

void pagesessions::on_sessionsView_itemSelectionChanged()
{
    bool checked = menu->isChecked();

    if ( ui->sessionsView->currentItem() ) {
        // FIX ME
        QSettings session( CPrime::Variables::CC_System_ConfigDir() + "coreapps/session", QSettings::IniFormat );
        QStringList sessionList = session.childGroups();

        if ( sessionList.contains( ui->sessionsView->currentItem()->text( 0 ) ) ) {
            m_editSession->setVisible( checked );
            m_deleteSession->setVisible( checked );
            m_addSession->setVisible( checked );
        } else {
            m_editSession->setVisible( 0 );
            m_deleteSession->setVisible( 0 );
            m_addSession->setVisible( checked );
        }
    } else {
        m_editSession->setVisible( 0 );
        m_deleteSession->setVisible( 0 );
        m_addSession->setVisible( checked );
    }
}
