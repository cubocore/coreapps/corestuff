/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include "button.h"

#include <QApplication>
#include <QDebug>
#include <QAction>

#include <KF6/KGlobalAccel/kglobalaccel.h>


DockButton::DockButton() {

    isDockHide = true;

	settings smi;

    // setup button
	this->setFixedSize(smi.getValue("CoreApps", "ToolsIconSize") + QSize(10,10));
	resize(smi.getValue("CoreApps", "ToolsIconSize") + QSize(10,10));
    this->setIcon(QIcon::fromTheme("cc.cubocore.CoreStuff"));
	this->setIconSize(smi.getValue("CoreApps", "ToolsIconSize"));
    this->move(50,50);

    // setup the window
    setWindowFlags( Qt::Window | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint /*| Qt::X11BypassWindowManagerHint*/ );
    setFocusPolicy( Qt::NoFocus );

	// Will be shown on all desktops, cannot be closed using Alt+F4
    setAttribute( Qt::WA_X11NetWmWindowTypeDock );			// Qt specifies that this has no effect on platforms other than X11, so it should not matter.
    this->setWindowOpacity(.90);

    cstuff = new corestuff();
	cstuff->show();

    connect( cstuff, SIGNAL( activateDock() ), this, SLOT( animateClick() ) );
	connect( this, &DockButton::clicked, this, &DockButton::onClick );

    QAction *activateDock = new QAction( QIcon::fromTheme( "cc.cubocore.CoreStuff" ), "Activate Dock", this );
	activateDock->setObjectName( "Dock Activator" );
	connect( activateDock, &QAction::triggered, this, &QPushButton::animateClick );

    QKeySequence seq( Qt::Key_Super_L );
	if ( not KGlobalAccel::self()->setShortcut( activateDock, { seq }, KGlobalAccel::NoAutoloading ) )
		qDebug() << "Failed to register 'Meta' as a global shortcut for CoreStuff Dock";

	addAction( activateDock );
}

DockButton::~DockButton()
{
    cstuff->deleteLater();
}

void DockButton::onClick()
{
    cstuff->showTasksPage();
    cstuff->showDesktopT();
}

void DockButton::mousePressEvent(QMouseEvent *event)
{
    if (underMouse()) {
        mousePos = event->pos();
    }

    QPushButton::mousePressEvent(event);
}

void DockButton::mouseMoveEvent(QMouseEvent *event)
{
    QPointF globalPos = event->globalPosition();
    int mousePointx = (int)(globalPos.x() - mousePos.x());
    int mousePointy = (int)(globalPos.y() - mousePos.y());
    move(mousePointx, mousePointy);
}
