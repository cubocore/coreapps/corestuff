#include <QtGui>

#include <stdlib.h>
#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/X.h>
#include <X11/extensions/Xcomposite.h>

void Save_XImage_to_PNG( XImage* xi, QString filename ) {

	QImage::Format format = QImage::Format_ARGB32_Premultiplied;

	if (xi->depth == 24)
		format = QImage::Format_RGB32;

	else if (xi->depth == 16)
		format = QImage::Format_RGB16;

	QImage image = QImage(reinterpret_cast<uchar*>(xi->data), xi->width, xi->height, xi->bytes_per_line, format).copy();

	// we may have to swap the byte order
	if ((QSysInfo::ByteOrder == QSysInfo::LittleEndian && xi->byte_order == MSBFirst)
		|| (QSysInfo::ByteOrder == QSysInfo::BigEndian && xi->byte_order == LSBFirst)) {

		for (int i = 0; i < image.height(); i++) {
			if (xi->depth == 16) {
				ushort* p = reinterpret_cast<ushort*>(image.scanLine(i));
				ushort* end = p + image.width();
				while (p < end) {
					*p = ((*p << 8) & 0xff00) | ((*p >> 8) & 0x00ff);
					p++;
				}
			}

			else {
				uint* p = reinterpret_cast<uint*>(image.scanLine(i));
				uint* end = p + image.width();
				while (p < end) {
					*p = ((*p << 24) & 0xff000000) | ((*p << 8) & 0x00ff0000)
						 | ((*p >> 8) & 0x0000ff00) | ((*p >> 24) & 0x000000ff);
					p++;
				}
			}
		}
	}

	// fix-up alpha channel
	if (format == QImage::Format_RGB32) {
		QRgb* p = reinterpret_cast<QRgb*>(image.bits());
		for (int y = 0; y < xi->height; ++y) {
			for (int x = 0; x < xi->width; ++x)
				p[x] |= 0xff000000;
			p += xi->bytes_per_line / 4;
		}
	}

	image.save( filename );
}

void CaptureWindow( XID winID, QString filename ) {

	Display *display = XOpenDisplay( NULL );
	XID xid = winID;

	// Check if Composite extension is enabled
	int event_base_return;
	int error_base_return;
	if( XCompositeQueryExtension( display, &event_base_return, &error_base_return ) )
		printf( "COMPOSITE IS ENABLED!\n" );

	// Requests the X server to direct the hierarchy starting at window to off-screen storage
	XCompositeRedirectWindow( display, xid, CompositeRedirectAutomatic );
	// Preventing the backing pixmap from being freed when the window is hidden/destroyed
	// If you want the window contents to still be available after the window has been destroyed,
	// or after the window has been resized( but not yet redrawn ), you can increment the backing
	// pixmaps ref count to prevent it from being deallocated.
	Pixmap pixmap = XCompositeNameWindowPixmap( display, xid );

	// Get window attributes
	XWindowAttributes attr;
	Status s = XGetWindowAttributes( display, xid, &attr );
	if( s == 0 )
		printf( "Fail to get window attributes!\n" );

	// Extract the data
	int width = attr.width;
	int height = attr.height;
	int depth = attr.depth;

	// Since we already have the pixmap, we can use it. We will use XGetImage to obtain the pixels
	// We can then use those pixels and store them as a jpeg image.
	XImage *image = XGetImage( display, pixmap, 0, 0, width, height, AllPlanes, ZPixmap );
	Save_XImage_to_PNG( image, filename );
	XDestroyImage( image );

	XFreePixmap( display, pixmap );
};

void CaptureScreen( QString filename ) {

	Display *display = XOpenDisplay( NULL );
	Window root = DefaultRootWindow(display);

	XWindowAttributes gwa;
	XGetWindowAttributes(display, root, &gwa);

	int width = gwa.width;
	int height = gwa.height;

	XImage *image = XGetImage( display, root, 0, 0, width, height, AllPlanes, ZPixmap );

	// Since we already have the pixmap, we can use it. We will use XGetImage to obtain the pixels
	// We can then use those pixels and store them as a jpeg image.
	Save_XImage_to_PNG( image, filename );
	XDestroyImage( image );
};

int main(  int argc, char *argv[]  ) {

	QGuiApplication *app = new QGuiApplication( argc, argv );

	if ( argc < 3 ) {
		qDebug() << "Usage:";
		qDebug() << "    screengrab --window  windowID filename.ext";
		qDebug() << "    screengrab --screen filename.ext";
	}

	qDebug() << strcmp( argv[ 1 ], "--window" );

	if ( strcmp( argv[ 1 ], "--window" ) == 0 )
		CaptureWindow( atoi( argv[ 2 ] ), argv[ 3 ] );

	else
		CaptureScreen( argv[ 2 ] );

	return 0;
};
