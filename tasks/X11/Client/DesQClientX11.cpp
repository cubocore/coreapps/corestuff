/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* ( at your option ) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QtCore>
#include <QtGui>
#include <qpa/qplatformnativeinterface.h>

#include "DesQWMSession.hpp"
#include "DesQClient.hpp"
#include "DesQClientX11.hpp"

#include <X11/Xlib.h>
#include <X11/X.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xcomposite.h>

static inline int getAppScreen() {
    // Get the X11 display
    Display* display = static_cast<Display*>(QGuiApplication::platformNativeInterface()->nativeResourceForIntegration("display"));
    if (!display) {
        qWarning("Unable to retrieve X11 display.");
        return -1;
    }

    // Get the default screen number
    return DefaultScreen(display);
}

static inline Window getAppRootWindow() {
    Display* display = static_cast<Display*>(QGuiApplication::platformNativeInterface()->nativeResourceForIntegration("display"));
    if (!display) {
        qWarning("Unable to retrieve X11 display.");
        return 0;
    }

    return DefaultRootWindow(display);
}

static inline unsigned long getAppUserTime() {
    Display* display = static_cast<Display*>(QGuiApplication::platformNativeInterface()->nativeResourceForIntegration("display"));
    if (display == nullptr) {
        return 0;
    }

    int time = 0;
    XGetInputFocus(display, None, &time);

    return time;
}

DesQClientPrivate::DesQClientPrivate( DesQClient *dc ) {

	XCB = qApp->nativeInterface<QNativeInterface::QX11Application>()->connection();

	xcb_intern_atom_cookie_t *cookie = xcb_ewmh_init_atoms( XCB, &EWMH );
    mEWMHValid = ( bool )xcb_ewmh_init_atoms_replies( &EWMH, cookie, nullptr );

	/* Reference to DesQClient* object */
	q = dc;

	mType = DesQClient::DesQClientType::Normal;
	mWid = 0;
	mPid = 0;
	mDesktop = 0;
	mTitle = QString();
};

DesQClient::DesQClient( Window WId ) : QObject() {

	d = new DesQClientPrivate( this );
	d->mWid = WId;

	/* PID and Desktop number */
    xcb_ewmh_get_wm_pid_reply( &d->EWMH, xcb_ewmh_get_wm_pid( &d->EWMH, WId ), &d->mPid, nullptr );
    xcb_ewmh_get_wm_desktop_reply( &d->EWMH, xcb_ewmh_get_wm_desktop( &d->EWMH, WId ), &d->mDesktop, nullptr );

	/* Title */
	xcb_get_property_cookie_t netWMameCookie = xcb_get_property( d->XCB, false, WId, d->EWMH._NET_WM_NAME, d->EWMH.UTF8_STRING, 0, BUFSIZ );
    xcb_get_property_reply_t *replyNetWM = xcb_get_property_reply( d->XCB, netWMameCookie, nullptr );

	char *wm_name = {};
	int wm_name_len = 0;

	if ( replyNetWM && ( replyNetWM->type != XCB_NONE ) ) {
		wm_name = ( char * )xcb_get_property_value( replyNetWM );
		wm_name_len = xcb_get_property_value_length( replyNetWM );
	}

	else {
		xcb_get_property_cookie_t nameCookie = xcb_get_property( d->XCB, false, WId, d->EWMH._NET_WM_VISIBLE_NAME, d->EWMH.UTF8_STRING, 0, BUFSIZ );
        xcb_get_property_reply_t *reply = xcb_get_property_reply( d->XCB, nameCookie, nullptr );

		if ( reply && ( reply->type != XCB_NONE ) ) {
			wm_name = ( char * )xcb_get_property_value( reply );
			wm_name_len = xcb_get_property_value_length( reply );
		}
	}

	if ( wm_name_len > 0 )
		d->mTitle = QString::fromLocal8Bit( wm_name, wm_name_len );

	d->mIcon = getIcon( WId );
	d->mType = getType( WId );
};

DesQClient::DesQClient( const DesQClient& other ) : QObject() {

	d = new DesQClientPrivate( this );

	d->mType = other.type();
	d->mPid = other.pid();
	d->mDesktop = other.desktop();
	d->mWid = other.wid();
	d->mIcon = other.icon();
	d->mTitle = other.title();
};

qulong DesQClient::type() const {

	return d->mType;
};

qulong DesQClient::pid() const {

	return d->mPid;
};

qulong DesQClient::wid() const {

	return d->mWid;
};

int DesQClient::desktop() const {

	return d->mDesktop;
};

QString DesQClient::title() const {

	return d->mTitle;
};

QIcon DesQClient::icon() const {

	return d->mIcon;
};

QString DesQClient::typeStr() const {

	switch( d->mType ) {
		case None:
			return QString();

		case Desktop:
			return QString( "Desktop" );

		case Dock:
			return QString( "Dock" );

		case Toolbar:
			return QString( "ToolBar" );

		case Menu:
			return QString( "Menu" );

		case Utility:
			return QString( "Utility" );

		case Splash:
			return QString( "SplashScreen" );

		case Dialog:
			return QString( "Dialog" );

		case Dropdown_menu:
			return QString( "DropDownMenu" );

		case Popup_menu:
			return QString( "PopupMenu" );

		case Tooltip:
			return QString( "Tooltip" );

		case Notification:
			return QString( "Notification" );

		case Combo:
			return QString( "Combo" );

		case Dnd:
			return QString( "DnD" );

		case Normal:
			return QString( "Normal" );
	}

	return QString( "Normal" );
};

QImage DesQClient::screengrab() const {

	Display *display = XOpenDisplay( NULL );
	XID xid = d->mWid;

	// Check if Composite extension is enabled
	int event_base_return;
	int error_base_return;
	if ( not XCompositeQueryExtension( display, &event_base_return, &error_base_return ) )
		qDebug() << "Expect bad quality screengrabs";

	// Requests the X server to direct the hierarchy starting at window to off-screen storage
	XCompositeRedirectWindow( display, xid, CompositeRedirectAutomatic );
	// Preventing the backing pixmap from being freed when the window is hidden/destroyed
	// If you want the window contents to still be available after the window has been destroyed,
	// or after the window has been resized( but not yet redrawn ), you can increment the backing
	// pixmaps ref count to prevent it from being deallocated.
	Pixmap pixmap = XCompositeNameWindowPixmap( display, xid );

	// Get window attributes
	XWindowAttributes attr;
	if ( not XGetWindowAttributes( display, xid, &attr ) )
		qDebug() << "Failed to get window attributes";

	// Extract the data
	int width = attr.width;
	int height = attr.height;

	// Since we already have the pixmap, we can use it. We will use XGetImage to obtain the pixels
	// We can then use those pixels and store them as a jpeg image.
	XImage *image = XGetImage( display, pixmap, 0, 0, width, height, AllPlanes, ZPixmap );
	QImage screengrab;
	if ( image != NULL ) {
		screengrab = XImage2QImage( image );
		XDestroyImage( image );
	}

	else {
		screengrab = QImage( QSize( width, height ), QImage::Format_ARGB32 );
		screengrab.fill( Qt::white );
		QPixmap icon = d->mIcon.pixmap( qMin( width, height ) / 2 );
		QPainter p( &screengrab );
		p.setRenderHint( QPainter::Antialiasing );
		p.drawPixmap( QRect( ( width - 64 ) / 2, ( height - 64 ) / 2, 64, 64 ), icon );
		p.end();
	}

	XFreePixmap( display, pixmap );

	return screengrab;
};

bool DesQClient::isMinimized() const {

	xcb_atom_t wm_state = GetAtom( "_NET_WM_STATE" );
	xcb_get_property_cookie_t stateCookie = xcb_get_property( d->XCB, false, d->mWid, wm_state, XCB_GET_PROPERTY_TYPE_ANY, 0, BUFSIZ );
    xcb_get_property_reply_t *reply = xcb_get_property_reply( d->XCB, stateCookie, nullptr );

	if ( reply && ( reply->type != XCB_NONE ) && ( reply->value_len > 0 ) ) {
	    xcb_atom_t *atoms = ( xcb_atom_t * )xcb_get_property_value( reply );
	    int numItems = reply->value_len;

		for( int i = 0; i < numItems; i++ ) {
			if ( atoms[ i ] == d->EWMH._NET_WM_STATE_HIDDEN )
				return true;
		}
	}

	return false;
};

bool DesQClient::isMaximized() const {

	xcb_atom_t wm_state = GetAtom( "_NET_WM_STATE" );
	xcb_get_property_cookie_t stateCookie = xcb_get_property( d->XCB, false, d->mWid, wm_state, XCB_GET_PROPERTY_TYPE_ANY, 0, BUFSIZ );
    xcb_get_property_reply_t *reply = xcb_get_property_reply( d->XCB, stateCookie, nullptr );

	bool min = false, maxv = false, maxh = false;
	if ( reply && ( reply->type != XCB_NONE ) && ( reply->value_len > 0 ) ) {
	    xcb_atom_t *atoms = ( xcb_atom_t * )xcb_get_property_value( reply );
	    int numItems = reply->value_len;

		for( int i = 0; i < numItems; i++ ) {
			if ( atoms[ i ] == d->EWMH._NET_WM_STATE_HIDDEN )
				min = true;

			if ( atoms[ i ] == d->EWMH._NET_WM_STATE_MAXIMIZED_HORZ )
				maxh = true;

			if ( atoms[ i ] == d->EWMH._NET_WM_STATE_MAXIMIZED_VERT )
				maxv = true;
		}
	}

	/* We have a maximized window if its not minimized and has V and H maximized */
	return ( not min and ( maxv and maxh ) );
};

bool DesQClient::isActiveWindow() const {

	Window focus = DesQWMSession::currentSession()->getActiveWindow();

	if ( focus == d->mWid )
		return true;

	return false;
};

void DesQClient::activate() {

    xcb_client_message_event_t event;

	event.response_type = XCB_CLIENT_MESSAGE;
	event.format = 32;
	event.sequence = 0;
	event.window = d->mWid;
	event.type = GetAtom( "_NET_ACTIVE_WINDOW" );
	event.data.data32[0] = 2L;
	event.data.data32[1] = getAppUserTime();

    xcb_send_event( d->XCB, 0, getAppRootWindow(), XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT, (const char *)&event );
	xcb_flush( d->XCB );
};

void DesQClient::minimize() {

    xcb_client_message_event_t event;

	event.response_type = XCB_CLIENT_MESSAGE;
	event.format = 32;
	event.sequence = 0;
	event.window = d->mWid;
	event.type = GetAtom( "WM_CHANGE_STATE" );
	event.data.data32[0] = XCB_ICCCM_WM_STATE_ICONIC;
	event.data.data32[1] = 0;
	event.data.data32[2] = 0;
	event.data.data32[3] = 0;
	event.data.data32[4] = 0;

    xcb_send_event( d->XCB, 0, getAppRootWindow(), XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT, (const char *)&event );
	xcb_flush( d->XCB );
};

void DesQClient::restore() {

	/* If minimized, lets first activate it, and then perform the toggle */
	if ( isMinimized() )
		xcb_map_window( d->XCB, d->mWid );

	else {
		xcb_ewmh_request_change_wm_state(
			&d->EWMH,
			getAppScreen(),
			d->mWid,
			XCB_EWMH_WM_STATE_REMOVE,
			d->EWMH._NET_WM_STATE_MAXIMIZED_VERT,
			d->EWMH._NET_WM_STATE_MAXIMIZED_HORZ,
			XCB_EWMH_CLIENT_SOURCE_TYPE_NORMAL
		);
	}

	xcb_flush( d->XCB );
};

void DesQClient::maximize() {

	// /* If minimized, lets first activate it, and then perform the toggle */
	if ( isMinimized() )
		xcb_map_window( d->XCB, d->mWid );

	xcb_ewmh_request_change_wm_state(
		&d->EWMH,
		getAppScreen(),
		d->mWid,
		XCB_EWMH_WM_STATE_ADD,
		d->EWMH._NET_WM_STATE_MAXIMIZED_VERT,
		d->EWMH._NET_WM_STATE_MAXIMIZED_HORZ,
		XCB_EWMH_CLIENT_SOURCE_TYPE_NORMAL
	);

	xcb_flush( d->XCB );
};

void DesQClient::close() {

	xcb_ewmh_request_close_window(
		&d->EWMH,
		getAppScreen(),
		d->mWid,
		0,
		XCB_EWMH_CLIENT_SOURCE_TYPE_NORMAL
	);

	xcb_flush( d->XCB );
};

void DesQClient::moveToDesktop( int desktop ) {

	xcb_ewmh_request_change_wm_desktop(
		&d->EWMH,
		getAppScreen(),
		d->mWid,
		( quint32 )desktop,
		XCB_EWMH_CLIENT_SOURCE_TYPE_NORMAL
	);

	xcb_flush( d->XCB );
}
