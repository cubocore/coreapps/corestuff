/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* ( at your option ) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <DesQClient.hpp>
#include "DesQGlobals.hpp"
#include "X11Helpers.hpp"

#ifdef None
#ifndef FIXX11H_None
#define FIXX11H_None
const XID XNone = None;
#undef None
const XID None = XNone;
#endif
#undef None
#endif

QString GetAtomName( xcb_atom_t atm ) {

	xcb_connection_t *disp = qApp->nativeInterface<QNativeInterface::QX11Application>()->connection();

    xcb_get_atom_name_reply_t *reply = xcb_get_atom_name_reply( disp, xcb_get_atom_name( disp, atm ), nullptr );
	const char *name = xcb_get_atom_name_name( reply );

	return QString::fromLocal8Bit( name );
};

xcb_atom_t GetAtom( const char *name ) {

    xcb_intern_atom_cookie_t atom_cookie;
    xcb_atom_t atom = XCB_ATOM_NONE;
    xcb_intern_atom_reply_t *rep;

    xcb_connection_t *disp = qApp->nativeInterface<QNativeInterface::QX11Application>()->connection();

    atom_cookie = xcb_intern_atom( disp, 0, strlen( name ), name );
    rep = xcb_intern_atom_reply( disp, atom_cookie, nullptr );
    if ( NULL != rep ) {
        atom = rep->atom;
        free( rep );
    }

    return atom;
};

QIcon getIcon( Window mWid ) {

	QIcon mIcon;
	mIcon.addPixmap( QIcon::fromTheme( "application-x-executable" ).pixmap( 48 ) );

	xcb_ewmh_connection_t EWMH;
	xcb_intern_atom_cookie_t *ewmhCookie = xcb_ewmh_init_atoms( qApp->nativeInterface<QNativeInterface::QX11Application>()->connection(), &EWMH );
	bool mEWMHValid = ( bool )xcb_ewmh_init_atoms_replies( &EWMH, ewmhCookie, nullptr );

	xcb_get_property_cookie_t cookie = xcb_ewmh_get_wm_icon_unchecked( &EWMH, mWid );
	xcb_ewmh_get_wm_icon_reply_t reply;

	if ( not mEWMHValid )
		return mIcon;

    if ( xcb_ewmh_get_wm_icon_reply( &EWMH, cookie, &reply, nullptr ) ) {
		xcb_ewmh_wm_icon_iterator_t iter = xcb_ewmh_get_wm_icon_iterator(&reply);

		//Just use the first
		bool done = false;
		while ( not done ) {
			//Now convert the current data into a Qt image
			// - first 2 elements are width and height (removed via XCB functions)
			// - data in rows from left to right and top to bottom
			QImage image( iter.width, iter.height, QImage::Format_ARGB32 ); //initial setup
			uint* dat = iter.data;

			#if QT_VERSION <= QT_VERSION_CHECK(5, 10, 0)
				for ( int i = 0; i < image.byteCount() / 4; ++i, ++dat )
			#else
				for ( int i = 0; i < image.sizeInBytes() / 4; ++i, ++dat )
			#endif
					( ( uint* )image.bits() )[ i ] = *dat;

			mIcon.addPixmap( QPixmap::fromImage( image ) );

			// icon.addPixmap( QPixmap::fromImage( image ) ); //layer this pixmap onto the icon
			//Now see if there are any more icons available
			done = ( iter.rem < 1 ); //number of icons remaining
			if ( not done )
				xcb_ewmh_get_wm_icon_next( &iter ); //get the next icon data
		}

		xcb_ewmh_get_wm_icon_reply_wipe( &reply );
	}

	return mIcon;
};

qulong getType( Window mWid ) {

	qulong mType = DesQClient::DesQClientType::None;
	xcb_connection_t *XCB = qApp->nativeInterface<QNativeInterface::QX11Application>()->connection();

	xcb_atom_t propDesktop			= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_DESKTOP" );
	xcb_atom_t propDock				= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_DOCK" );
	xcb_atom_t propToolbar			= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_TOOLBAR" );
	xcb_atom_t propMenu				= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_MENU" );
	xcb_atom_t propUtility			= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_UTILITY" );
	xcb_atom_t propSplash			= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_SPLASH" );
	xcb_atom_t propDialog			= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_DIALOG" );
	xcb_atom_t propDropdown_menu	= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_DROPDOWN_MENU" );
	xcb_atom_t propPopup_menu		= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_POPUP_MENU" );
	xcb_atom_t propTooltip			= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_TOOLTIP" );
	xcb_atom_t propNotification 	= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_NOTIFICATION" );
	xcb_atom_t propCombo			= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_COMBO" );
	xcb_atom_t propDnd				= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_DND" );
	xcb_atom_t propNormal			= GetAtom( ( const char * )"_NET_WM_WINDOW_TYPE_NORMAL" );

	xcb_atom_t atom = GetAtom( "_NET_WM_WINDOW_TYPE" );
	xcb_get_property_cookie_t cookie = xcb_get_property( XCB, false, mWid, atom, XCB_GET_PROPERTY_TYPE_ANY, 0, BUFSIZ );
    xcb_get_property_reply_t *reply = xcb_get_property_reply( XCB, cookie, nullptr );

	if ( reply && ( reply->type != XCB_NONE ) && ( reply->value_len > 0 ) ) {
	    xcb_atom_t *atoms = ( xcb_atom_t * )xcb_get_property_value( reply );
	    int numItems = reply->value_len;

		/*
			* Multiple window types can be set, example, Frameless Qt Window in KDE
			* KDE sets _KDE_NET_WM_WINDOW_TYPE_OVERRIDE and _NET_WM_WINDOW_TYPE_NORMAL
			* In such cases we look for and use the first defined type
		*/
		for( int i = 0; i < numItems; i++ ) {
			xcb_atom_t pprty = atoms[ i ];

			if ( pprty == propDesktop )
				mType = DesQClient::DesQClientType::Desktop;

			else if ( pprty == propDock )
				mType = DesQClient::DesQClientType::Dock;

			else if ( pprty == propToolbar )
				mType = DesQClient::DesQClientType::Toolbar;

			else if ( pprty == propMenu )
				mType = DesQClient::DesQClientType::Menu;

			else if ( pprty == propUtility )
				mType = DesQClient::DesQClientType::Utility;

			else if ( pprty == propSplash )
				mType = DesQClient::DesQClientType::Splash;

			else if ( pprty == propDialog )
				mType = DesQClient::DesQClientType::Dialog;

			else if ( pprty == propDropdown_menu )
				mType = DesQClient::DesQClientType::Dropdown_menu;

			else if ( pprty == propPopup_menu )
				mType = DesQClient::DesQClientType::Popup_menu;

			else if ( pprty == propTooltip )
				mType = DesQClient::DesQClientType::Tooltip;

			else if ( pprty == propNotification )
				mType = DesQClient::DesQClientType::Notification;

			else if ( pprty == propCombo )
				mType = DesQClient::DesQClientType::Combo;

			else if ( pprty == propDnd )
				mType = DesQClient::DesQClientType::Dnd;

			else if ( pprty == propNormal )
				mType = DesQClient::DesQClientType::Normal;

			if ( mType != DesQClient::DesQClientType::None )
				break;
		}
	}

	return mType;
};

QImage XImage2QImage( XImage* xi ) {

	QImage::Format format = QImage::Format_ARGB32_Premultiplied;

	if ( xi->depth == 24 )
		format = QImage::Format_RGB32;

	else if ( xi->depth == 16 )
		format = QImage::Format_RGB16;

	QImage image = QImage( reinterpret_cast<uchar*>( xi->data ), xi->width, xi->height, xi->bytes_per_line, format ).copy();

	// we may have to swap the byte order
	if ( ( QSysInfo::ByteOrder == QSysInfo::LittleEndian && xi->byte_order == MSBFirst )
		|| ( QSysInfo::ByteOrder == QSysInfo::BigEndian && xi->byte_order == LSBFirst ) ) {

		for ( int i = 0; i < image.height(); i++ ) {
			if (xi->depth == 16) {
				ushort* p = reinterpret_cast<ushort*>(image.scanLine(i));
				ushort* end = p + image.width();
				while (p < end) {
					*p = ((*p << 8) & 0xff00) | ((*p >> 8) & 0x00ff);
					p++;
				}
			}

			else {
				uint* p = reinterpret_cast<uint*>(image.scanLine(i));
				uint* end = p + image.width();
				while ( p < end ) {
					*p = ( ( *p << 24 ) & 0xff000000 ) | ( ( *p << 8 ) & 0x00ff0000 )
						 | ( ( *p >> 8 ) & 0x0000ff00 ) | ( ( *p >> 24 ) & 0x000000ff );
					p++;
				}
			}
		}
	}

	// fix-up alpha channel
	if ( format == QImage::Format_RGB32 ) {
		QRgb* p = reinterpret_cast<QRgb*>( image.bits() );
		for ( int y = 0; y < xi->height; ++y ) {
			for ( int x = 0; x < xi->width; ++x )
				p[ x ] |= 0xff000000;
			p += xi->bytes_per_line / 4;
		}
	}

	return image;
};
