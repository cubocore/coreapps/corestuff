/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QPainter>
#include <QScroller>

#include <cprime/pinmanage.h>
#include <cprime/cprime.h>
#include <cprime/appopenfunc.h>

#include "pagepins.h"
#include "ui_pagepins.h"


pagepins::pagepins(QWidget *parent) :QWidget(parent)
  , ui(new Ui::pagepins)
  , smi(new settings)
{
    ui->setupUi(this);

    loadSettings();
    startSetup();
    setupPinsView();

}

pagepins::~pagepins()
{
	delete smi;
    delete ui;
}

void pagepins::reload()
{
    qDebug() << "Reloading pins...";
    setupPinsView();
}

/**
 * @brief Loads application settings
 */
void pagepins::loadSettings()
{
    iconViewIconSize = smi->getValue("CoreApps", "IconViewIconSize");
    uiMode = smi->getValue("CoreApps", "UIMode");
}

void pagepins::startSetup()
{
    ui->pinsView->setIconSize( iconViewIconSize );
    ui->pinsView->setGridSize( QSize( iconViewIconSize.width() * 3, iconViewIconSize.height() * 2 ) );

    if ( uiMode != 0 ) {
        QScroller::grabGesture(ui->pinsView, QScroller::LeftMouseButtonGesture);
        connect( ui->pinsView,&QListWidget::itemClicked,this,&pagepins::openSelectedPin);
    } else {
        connect( ui->pinsView,&QListWidget::itemDoubleClicked,this,&pagepins::openSelectedPin);
    }
}

void pagepins::setupPinsView()
{
    ui->pinsView->clear();

    PinManage pm;
    QStringList list = pm.getPinNames( "Speed Dial" );
    QStringList mList;
    mList.clear();

    QStringList dateTimeList;
    dateTimeList.clear();

    foreach ( QString s, list ) {
        dateTimeList.append( pm.piningTime( "Speed Dial", s ) );
    }

    CPrime::SortFunc::sortDateTime( dateTimeList );

    int count = list.count();
    int reverse = count - 1;

    for ( int i = 0; i < count; i++ ) {
        for ( int j = 0; j < count; j++ ) {
            QString bTime = pm.piningTime( "Speed Dial", list.at( j ) );

            if ( bTime.contains( dateTimeList.at( i ) ) ) {
                mList.insert( reverse, list.at( j ) );
                reverse--;
            }
        }
    }

    dateTimeList.clear();
    list.clear();

    for ( int i = 0; i < mList.count(); ++i ) {
        if ( i == 15 ) {
            return;
        } else {

            QIcon icon = CPrime::ThemeFunc::getFileIcon( pm.pinPath( "Speed Dial", mList.at( i ) ) );
            icon = CPrime::ThemeFunc::resizeIcon( icon, ui->pinsView->iconSize() );

            QListWidgetItem *item = new QListWidgetItem( icon, mList.at( i ) );
            item->setSizeHint( QSize( 110, 100 ) );
            ui->pinsView->addItem( item );
        }
    }

    if (ui->pinsView->count()) {
        ui->pinsView->setVisible(1);
        ui->welcome->setVisible(0);
    } else {
        ui->pinsView->setVisible(0);
        ui->welcome->setVisible(1);
    }
}

void pagepins::openSelectedPin( QListWidgetItem *item )  // open SpeedDial on doubleclick
{
    PinManage pm;
    CPrime::AppOpenFunc::appOpenEngine( pm.pinPath( "Speed Dial", item->text() ) );
}
