/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QLabel>
#include <QScreen>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QApplication>

#include <cprime/themefunc.h>

#include "settings.h"

#include "powerdlg.h"


PowerDialog::PowerDialog( QWidget *parent ) : QDialog( parent )
{
	settings smi;
	setWindowFlags( Qt::Dialog | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint );
	setWindowModality( Qt::ApplicationModal );

	QSize toolBarIconSize = smi.getValue("CoreApps", "ToolsIconSize");

	powerDBus = new Power( this );

	logOutBtn = new QCommandLinkButton( "Logout", "Logout as the current user" );
    logOutBtn->setIcon( CPrime::ThemeFunc::themeIcon( "system-log-out-symbolic", "system-log-out", "system-log-out" ));
    logOutBtn->setIconSize(toolBarIconSize);
	connect( logOutBtn, &QCommandLinkButton::clicked, [=]() {
		QDialog::close();			// We do not want this dialog to appear while saving the session
		parent->close();			// We do not want corestuff to appear while saving the session

		logout();

		/* We manually close this app */
		qApp->quit();
	} );

	suspendBtn = new QCommandLinkButton( "Suspend", "Suspend the system to RAM" );
    suspendBtn->setIcon( CPrime::ThemeFunc::themeIcon( "system-log-out-symbolic", "system-suspend", "system-suspend" ) );
    suspendBtn->setIconSize(toolBarIconSize);
	connect( suspendBtn, &QCommandLinkButton::clicked, powerDBus, &Power::systemSuspend );
	if ( not powerDBus->systemCanSuspend() )
        suspendBtn->setDisabled( true );

	hibernateBtn = new QCommandLinkButton( "Hibernate", "Suspend the system to Disk" );
    hibernateBtn->setIcon( CPrime::ThemeFunc::themeIcon( "system-log-out-symbolic", "system-suspend-hibernate", "system-suspend-hibernate" ) );
    hibernateBtn->setIconSize(toolBarIconSize);
	connect( hibernateBtn, &QCommandLinkButton::clicked, powerDBus, &Power::systemHibernate );
	if ( not powerDBus->systemCanHibernate() )
        hibernateBtn->setDisabled( true );

    powerOffBtn = new QCommandLinkButton( "Power Off", "Power off the system" );
    powerOffBtn->setIcon( CPrime::ThemeFunc::themeIcon( "system-shutdown-symbolic", "system-shutdown", "system-shutdown" ) );
    powerOffBtn->setIconSize(toolBarIconSize);
	connect( powerOffBtn, &QCommandLinkButton::clicked, powerDBus, &Power::systemHalt );
	if ( not powerDBus->systemCanHalt() )
        powerOffBtn->setDisabled( true );

    rebootBtn = new QCommandLinkButton( "Reboot", "Reboot the system" );
    rebootBtn->setIcon( CPrime::ThemeFunc::themeIcon( "system-reboot-symbolic", "system-reboot", "system-reboot" ) );
    rebootBtn->setIconSize(toolBarIconSize);
	connect( rebootBtn, &QCommandLinkButton::clicked, powerDBus, &Power::systemReboot );
	if ( not powerDBus->systemCanReboot() )
        rebootBtn->setDisabled( true );

    cancelBtn = new QCommandLinkButton( "Cancel", "Do nothing" );
    cancelBtn->setIcon( CPrime::ThemeFunc::themeIcon( "edit-delete-symbolic", "edit-delete", "edit-delete" ) );
    cancelBtn->setIconSize(toolBarIconSize);
	connect( cancelBtn, &QToolButton::clicked, this, &QDialog::reject );

    QVBoxLayout *lyt = new QVBoxLayout();
    lyt->setContentsMargins(5,5,5,5);
	lyt->addWidget( logOutBtn );
	lyt->addWidget( suspendBtn );
	lyt->addWidget( hibernateBtn );
	lyt->addWidget( powerOffBtn );
	lyt->addWidget( rebootBtn );
    lyt->addWidget( cancelBtn );

	setLayout( lyt );

    QSize scrnSize = QApplication::primaryScreen()->size();
    int h = ( scrnSize.width() - 250 ) / 2;
    int v = ( scrnSize.height() - 384 ) / 2;

    resize( scrnSize.width() * .15 , scrnSize.height() * .4);
    move( h, v );

	int uiMode = smi.getValue("CoreApps", "UIMode");

    if (uiMode != 0) {
        this->setWindowState(Qt::WindowMaximized);
    }
};

void PowerDialog::logout() {
	/* Most of the window managers listed here are taken from https://github.com/TheCynicalTeam/qt-logout/ */
	/* Some changes are made here for better DE integration */

    QString desktop = QString::fromLocal8Bit( qgetenv( "DESKTOP_SESSION" ) ).toLower();
	QProcess logoutProc;

    qDebug() << "Desktop Session:" << desktop;

    if (desktop.contains( "herbstluftwm" ))
        logoutProc.start( "herbstclient", { "quit" } );

    else if (desktop.contains( "bspwm" ))
        logoutProc.start( "pkill", { "bspwm" } );

    else if (desktop.contains( "jwm" ))
        logoutProc.start( "pkill", { "jwm" } );

    else if (desktop.contains( "openbox" ))
        logoutProc.start( "pkill", { "openbox" } );

    else if (desktop.contains( "awesome" ))
        logoutProc.start( "pkill", { "awesome" } );

    else if (desktop.contains( "qtile" ))
        logoutProc.start( "pkill", { "qtile" } );

    else if (desktop.contains( "xmonad" ))
        logoutProc.start( "pkill", { "xmonad" } );

    else if (desktop.contains( "dwm" ))
        logoutProc.start( "pkill", { "dwm" } );

    else if (desktop.contains( "i3" ))
        logoutProc.start( "i3-msg", { "exit" } );

    else if (desktop.contains( "i3-with-shmlog" ))
        logoutProc.start( "pkill", { "i3-with-shmlog" } );

    else if (desktop.contains( "lxqt" ))
        logoutProc.start( "lxqt-leave", { "--logout" } );

    else if (desktop.contains( "spectrwm" ))
        logoutProc.start( "pkill", { "spectrwm" } );

    else if (desktop.contains( "xfce" ))
		logoutProc.start( "xfce4-session-logout", QStringList() );

    else if (desktop.contains( "sway" ))
        logoutProc.start( "pkill", { "sway" } );

    else if (desktop.contains( "cubocore" ))
		logoutProc.start( "desq-session-x11", { "--logout" } );

    else if (desktop.contains( "desq" ))
		logoutProc.start( "desq-session-x11", { "--logout" } );

    else if (desktop.contains( "oyster" ))
		logoutProc.start( "desq-session-x11", { "--logout" } );

    else if (desktop.contains( "plasma" ))
		logoutProc.start( "loginctl", { "terminate-user", qgetenv( "USER" ).data() } );

    else if (desktop.contains( "gnome" ))
		logoutProc.start( "gnome-session-quit", { "--logout" } );

	logoutProc.waitForFinished();
}
