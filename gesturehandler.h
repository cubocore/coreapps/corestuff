/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *
    
    *
    * This file is taken from https://github.com/u8sand/Baka-MPlayer
    * Additional modifications has been made to meet the needs of CoreStuff.
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QObject>
#include <QElapsedTimer>
#include <QPoint>

class GestureHandler : public QObject
{
    Q_OBJECT

public:
    enum GESTURE_STATE {
        NONE,
        LEFT_TO_RIGHT,
        RIGHT_TO_LEFT,
        TOP_TO_BOTTOM,
        BOTTOM_TO_UP
    };

    explicit GestureHandler(QObject *parent = 0);
    ~GestureHandler();

public slots:
    bool Begin(QPoint mousePos);
    bool Process(QPoint mousePos);
    bool End();

private:
    QElapsedTimer *elapsedTimer;
    double hRatio, vRatio;
    int timer_threshold, gesture_threshold;
    int gesture_type, gesture_state;
    QPoint start;
};

