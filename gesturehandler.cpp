/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This file is taken from https://github.com/u8sand/Baka-MPlayer
    * Additional modifications has been made to meet the needs of CoreStuff.
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include "gesturehandler.h"

#include <QApplication>
#include <QScreen>
#include <QDebug>


GestureHandler::GestureHandler(QObject *parent):
    QObject(parent),
    elapsedTimer(nullptr)
{
    timer_threshold = 10; // 10ms works quite well
    gesture_threshold = 15; // 15 pixels is probably fine for anything
}

GestureHandler::~GestureHandler()
{
    if(elapsedTimer)
    {
        delete elapsedTimer;
        elapsedTimer = nullptr;
    }
}

bool GestureHandler::Begin(QPoint mousePos)
{
    if(!elapsedTimer)
    {
        elapsedTimer = new QElapsedTimer();
        elapsedTimer->start();
        this->gesture_state = NONE;
        // calculate pixel ratios based on physical dpi
        hRatio = qApp->primaryScreen()->physicalDotsPerInchX() / 800.0;
        vRatio = qApp->primaryScreen()->physicalDotsPerInchY() / 450.0;
    }
    else{
        return false;
    }

    QApplication::setOverrideCursor(QCursor(Qt::PointingHandCursor));

    start = mousePos;
    return true;
}

bool GestureHandler::Process(QPoint mousePos)
{
    if(elapsedTimer && elapsedTimer->elapsed() > timer_threshold) // 10ms seems pretty good for all purposes
    {
        QPoint delta = mousePos - start;

        switch(gesture_state)
        {
        case NONE:
            if(abs(delta.x()) >= abs(delta.y()) + gesture_threshold){
                int g = delta.x() * hRatio;
                if(g>0)
                    gesture_state = LEFT_TO_RIGHT;
                else
                    gesture_state = RIGHT_TO_LEFT;
            }
            else if(abs(delta.y()) >= abs(delta.x()) + gesture_threshold){
                int g = delta.y() * vRatio;
                if(g>0)
                    gesture_state = TOP_TO_BOTTOM;
                else
                    gesture_state = BOTTOM_TO_UP;
            }
            break;
        case LEFT_TO_RIGHT:
        {
            qDebug () << "33333333333333333333 LEFT_TO_RIGHT"  ;
//          corestuff::previousPage()
            break;
        }

        case RIGHT_TO_LEFT:
        {
            qDebug () << "33333333333333333333 RIGHT_TO_LEFT" ;
//          corestuff::previousPage()
            break;
        }

        case TOP_TO_BOTTOM:
        {
            qDebug () << "444444444444444444444444444 TOP_TO_BOTTOM";
            break;
        }
        case BOTTOM_TO_UP:
        {
            qDebug () << "444444444444444444444444444 BOTTOM_TO_UP" ;
            break;
        }

        elapsedTimer->restart();
        return true;
        }
    }

    return false;
}

bool GestureHandler::End()
{
    if(elapsedTimer)
    {
        delete elapsedTimer;
        elapsedTimer = nullptr;
        QApplication::restoreOverrideCursor();
    }
    else
        return false;
    return true;
}
