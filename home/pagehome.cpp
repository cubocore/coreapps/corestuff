/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QPainter>
#include <QDebug>
#include <QScreen>
#include <QTimer>
#include <QPluginLoader>
#include <QDateTime>
#include <QMouseEvent>
#include <QScroller>

#include <cprime/cplugininterface.h>
#include <cprime/cprime.h>
#include <cprime/systemxdg.h>
#include <cprime/messageengine.h>

#include "pagehome.h"
#include "ui_pagehome.h"


pagehome::pagehome(QWidget *parent) : QWidget(parent)
  , ui(new Ui::pagehome)
  , smi(new settings)
{
    ui->setupUi(this);

    QScreen *scrn = QGuiApplication::primaryScreen();
    loadSettings();
    startSetup();
    setupWidgetView(scrn->availableGeometry());

}

pagehome::~pagehome()
{
	delete smi;
    delete ui;
}

/**
 * @brief Loads application settings
 */
void pagehome::loadSettings()
{
    uiMode = smi->getValue("CoreApps", "UIMode");
    selectedPlugins.clear();
    selectedPlugins = smi->getValue("CoreAction", "SelectedPlugins");
}

void pagehome::setupWidgetView(QRect geometry)
{
    int width = geometry.width();
    int height = geometry.height();

    if (width > height) {
        qDebug() << "orientation: Landscape" ;

        // tablet mode
        if(uiMode == 1){
            qDebug() << "Landscape: tablet";
            x = static_cast<int>(width * .30);
            ui->day->hide();
            ui->time->show();
        }
        // mobile mode
        else if(uiMode == 2){
            qDebug() << "Landscape: mobile";
            x = static_cast<int>(width * .70);
            ui->sp->changeSize(QSizePolicy::Fixed, QSizePolicy::Expanding);
            ui->time->hide();
            ui->day->show();
        }
        // desktop mode
        else{
            qDebug() << "Landscape: desktop";
            x = static_cast<int>(width * .20);
            ui->day->hide();
            ui->time->show();
        }

    } else {
        qDebug() << "orientation: Portrait" ;

        // tablet mode
        if(uiMode == 1){
            qDebug() << "Portait: tablet ";
            x = static_cast<int>(width * .70);
            ui->time->hide();
            ui->day->show();
        }
        // mobile mode
        else if(uiMode == 2){
            qDebug() << "Portait: mobile";
            x = static_cast<int>(width);
            ui->day->hide();
            ui->time->show();
        }
        // desktop mode
        else{
            qDebug() << "Portait: desktop";
            x = static_cast<int>(width * .30);
            ui->day->hide();
            ui->time->show();
        }

    }

    for (int i = 0; i < ui->widgetLayout->count(); ++i) {
        QWidget *w = ui->widgetLayout->itemAt(i)->widget();

        if (w != NULL) {
            w->setFixedWidth(x - 3);
        }

        qDebug() << x;
    }

    // set coreaction widith
    ui->frame->setFixedWidth(x);
}

void pagehome::startSetup()
{
    // Detect screen rotation and take necessary steps
    QScreen *scrn = qApp->primaryScreen();//qApp->screens()[screenIndex];

    connect(scrn, &QScreen::availableGeometryChanged, [this, scrn](const QRect & geometry) {
        qDebug() << "Screen size Changed to: " << scrn->size();
        setupWidgetView(geometry);
    });

    if (uiMode != 0) {
        QScroller::grabGesture(ui->widgets, QScroller::LeftMouseButtonGesture);
    }


    updateTime();
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &pagehome::updateTime);
    timer->start(1000);

    if (!selectedPlugins.count()) {
        CPrime::MessageEngine::showMessage(
			"cc.cubocore.CoreStuff",
			"CoreStuff",
			"Plugins not found",
            "Please install plugins and ensure they are enabled in CoreGarage."
        );
    }

    Q_FOREACH (QString pPath, selectedPlugins) {
        QPluginLoader loader(pPath);
        QObject *pObject = loader.instance();

        if (pObject) {
            WidgetsInterface *plugin = qobject_cast<WidgetsInterface *>(pObject);

            if (!plugin) {
                qWarning() << "Plugin Error:" << loader.errorString();
                continue;
            }

            QWidget *w = plugin->widget(this);
            ui->widgetLayout->addWidget(w);
        } else {
            qWarning() << "Plugin Error:" << loader.errorString();
        }
    }

    ui->widgetLayout->addStretch();
}

void pagehome::updateTime()
{
    QDateTime dt = QDateTime::currentDateTime();
    ui->day->setText( "<span style=font-size:24pt;>" +dt.toString("hh:mm AP") + "</span><br/><span style=font-size:18pt;>" + dt.toString("ddd MMMM d ") + "</span><br/>" + qgetenv("USER"));
    ui->time->setText( "<span style=font-size:24pt;>" +dt.toString("hh:mm AP") + "</span><br/><span style=font-size:18pt;>" + dt.toString("ddd MMMM d ") + "</span><br/>" );
}
